source("GetReturns.R")
source("MeanVariance.R")
rf = 0.0175/252
outputfile=paste('OptimizeParams', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.dat', sep='')

insertSource("CovSest.R", package = "rrcov", functions = "CovSest")
insertSource("CovMve.R", package = "rrcov", functions = "CovMve")

ResultDir="Result"
dir.create(ResultDir, showWarnings=FALSE)

#alltickers = c("AAXJ", "ACWI", "ACWV", "AGG", "AGZ", "CMF", "DGRO", "DVYE", "EEMS", "EEMV", "EFAV", "EFG", "EFV", "EMB", "EWJ", "FBND", "FCOM", "FCOR", "FDIS", "FDLO", "FDMO", "FDRR", "FDVV", "FENY", "FHLC", "FIDU", "FLOT", "FLTB", "FM", "FMAT", "FNCL", "FQAL", "FREL", "FSTA", "FTEC", "FUTY", "FVAL", "GNMA", "GOVT", "GVI", "HDV", "HYG", "IAGG", "IDV", "IEF", "IEFA", "IEI", "IEMG", "IEUR", "IEV", "IFGL", "IGOV", "IJH", "IJJ", "IJK", "IJR", "IJS", "IJT", "ILF", "ILTB", "INDY", "IPAC", "ISTB", "ITOT", "IUSB", "IUSG", "IUSV", "IVE", "IVV", "IVW", "IWC", "IXUS", "LQD", "MBB", "MCHI", "MUB", "NYF", "ONEQ", "PFF", "RING", "SCZ", "SHV", "SHY", "SHYG", "SLQD", "SLVP", "STIP", "SUB", "TIP", "TLT", "USMV")
#alltickers=c("AAXJ", "ACWI", "ACWV", "AGG", "AGZ", "DVYE", "EEMS", "EEMV", "EFAV", "EMB", "EWJ", "FLOT", "GNMA", "GOVT", "GVI", "HDV", "HYG", "IDV", "IEF", "IEV", "IFGL", "IJH", "IJR", "ILF", "ILTB", "INDY", "IVV", "IWC", "LQD", "MBB", "MCHI", "PFF", "RING", "SCZ", "SHY", "SLVP", "TIP", "TLT", "USMV")
#alltickers = c("AAXJ", "ACWI", "AGG", "AGZ", "AOA", "AOK", "AOM", "AOR", "AUSE", "AXJL", "BAB", "BIL", "BIV", "BKF", "BLV", "BND", "BSV", "BWX", "CEW", "CIU", "CSJ", "CWB", "CYB", "DBA", "DBC", "DBO", "DBV", "DEM", "DES", "DEW", "DFE", "DFJ", "DGS", "DIM", "DJP", "DLS", "DNL", "DOL", "DON", "DOO", "DRW", "DTH", "DTN", "DWM", "DXJ", "EDV", "EES", "EFG", "EFV", "ELD", "EMB", "EMLC", "EPI", "EPS", "ERUS", "EWA", "EWC", "EWG", "EWI", "EWJ", "EWP", "EWQ", "EWU", "EWX", "EWZ", "EXT", "EZM", "EZU", "FAN", "FBT", "FCG", "FDN", "FEU", "FEZ", "FIW", "FLM", "FNI", "FTAG", "FTRI", "FVL", "FXD", "FXG", "FXH", "FXI", "FXL", "FXN", "FXO", "FXR", "FXU", "GII", "GMF", "GNR", "GRID", "GULF", "GVI", "GWX", "GXC", "HEDJ", "IBND", "IEI", "IFGL", "IJH", "IJR", "IJS", "ILF", "ILTB", "IOO", "IPE", "ITE", "ITM", "IVE", "IVV", "IWB", "IWD", "IWF", "IWN", "IWO", "IWP", "IWS", "IWV", "JNK", "KBE", "KIE", "LQD", "MBB", "MDYG", "MDYV", "MGK", "MUB", "OEF", "PCEF", "PCY", "PDP", "PGF", "PJP", "PRF", "PRFZ", "PSK", "PXH", "PZA", "QABA", "QCLN", "QTEC", "RALS", "RSX", "RWO", "RWR", "RWX", "SCZ", "SHM", "SHY", "SLYG", "SLYV", "SPAB", "SPEM", "SPLB", "SPLG", "SPTL", "SPTM", "SPYG", "SPYV", "STIP", "STPZ", "SUB", "TFI", "TIP", "TLT", "USRT", "VB", "VBK", "VBR", "VCIT", "VCLT", "VCSH", "VEA", "VEU", "VGIT", "VGK", "VGLT", "VIG", "VIXM", "VMBS", "VNQ", "VO", "VOE", "VOT", "VSS", "VT", "VTI", "VTV", "VUG", "VWO", "VXF", "VYM", "WDTI", "WIP", "XHB", "XSD", "XTN")
#alltickers = c('AAXJ', 'ACWI', 'AGG', 'AOA', 'AOK', 'AOM', 'AOR', 'BIV', 'BKF', 'BLV', 'BND', 'BSV', 'BWX', 'CIU', 'CSJ', 'DBC', 'DBO', 'DBV', 'DJP', 'EDV', 'EFG', 'EFV', 'EMLC', 'EPI', 'EWA', 'EWC', 'EWG', 'EWI', 'EWJ', 'EWP', 'EWQ', 'EWU', 'EWX', 'EWZ', 'FEZ', 'FXI', 'GUR', 'GVI', 'GXC', 'IEI', 'IJH', 'IJR', 'IJS', 'ILF', 'IOO', 'ITM', 'IVE', 'IVV', 'IWB', 'IWD', 'IWF', 'IWN', 'IWO', 'IWP', 'IWS', 'IWV', 'JNK', 'LQD', 'MBB', 'MGK', 'MUB', 'OEF', 'PCY', 'RSX', 'RWO', 'RWR', 'RWX', 'SCZ', 'SHM', 'SHY', 'STPZ', 'TFI', 'TIP', 'TLT', 'VB', 'VBK', 'VBR', 'VCIT', 'VCLT', 'VCSH', 'VEA', 'VEU', 'VGIT', 'VGK', 'VGLT', 'VGSH', 'VIG', 'VMBS', 'VNQ', 'VO', 'VOE', 'VOT', 'VSS', 'VT', 'VTI', 'VTV', 'VUG', 'VWO', 'VXF', 'VYM', 'WIP')
#alltickers = c("AAXJ", "ACWF", "ACWI", "ACWV", "AGG", "AGGY", "AGND", "AGZ", "AGZD", "AIRR", "ALD", "ALTS", "AOA", "AOK", "AOM", "AOR", "AUSE", "AXJL", "BAB", "BIL", "BIV", "BKF", "BLV", "BND", "BSV", "BTAL", "BWX", "CARZ", "CEMB", "CEW", "CHEP", "CIBR", "CIU", "CJNK", "CSJ", "CWB", "CXSE", "CYB", "DBA", "DBC", "DBO", "DBV", "DDEZ", "DDJP", "DDLS", "DDWM", "DEM", "DES", "DEW", "DFE", "DFJ", "DGRE", "DGRS", "DGS", "DIM", "DJP", "DLS", "DNL", "DOL", "DON", "DOO", "DRW", "DTH", "DTN", "DVEM", "DWIN", "DWM", "DXGE", "DXJ", "DXJC", "DXJF", "DXJH", "DXJR", "DXJS", "DXPS", "DXUS", "DYB", "DYLS", "EDIV", "EDOM", "EDV", "EEMX", "EES", "EFAX", "EFG", "EFV", "ELD", "EMB", "EMCG", "EMDV", "EMHY", "EMLC", "EMLP", "EMSD", "EMTL", "EPI", "EPS", "ERUS", "ESGD", "ESGE", "ESGU", "EUDG", "EUSC", "EWA", "EWC", "EWG", "EWI", "EWJ", "EWP", "EWQ", "EWU", "EWX", "EWZ", "EXT", "EZM", "EZU", "FAAR", "FALN", "FAN", "FBT", "FBZ", "FCAN", "FCEF", "FCG", "FCVT", "FDN", "FEU", "FEZ", "FGM", "FIW", "FKO", "FLM", "FLN", "FLOT", "FLRN", "FMB", "FNI", "FONE", "FPA", "FPE", "FPEI", "FSZ", "FTAG", "FTGC", "FTRI", "FTSL", "FTSM", "FTXD", "FTXG", "FTXH", "FTXL", "FTXN", "FTXO", "FTXR", "FV", "FVL", "FXD", "FXG", "FXH", "FXI", "FXL", "FXN", "FXO", "FXR", "FXU", "GAL", "GII", "GMF", "GNR", "GOVT", "GRID", "GSD", "GULF", "GVI", "GWX", "GXC", "HDG", "HDMV", "HDRW", "HEDJ", "HEWJ", "HEZU", "HGSD", "HYLS", "HYMB", "HYND", "HYZD", "IAGG", "IBND", "IDMO", "IEI", "IEUR", "IFGL", "IHDG", "IJH", "IJR", "IJS", "ILF", "ILTB", "IMTM", "IOO", "IPAC", "IPE", "IPFF", "IPKW", "IQDG", "IQLT", "ITE", "ITM", "IVE", "IVV", "IWB", "IWD", "IWF", "IWN", "IWO", "IWP", "IWS", "IWV", "IYLD", "JNK", "JPGB", "JPGE", "JPHF", "JPHY", "JPST", "KBE", "KIE", "LALT", "LEMB", "LGLV", "LMBS", "LOWC", "LQD", "MBB", "MCEF", "MCHI", "MDYG", "MDYV", "MEAR", "MGK", "MOM", "MRGR", "MTUM", "MUB", "NEAR", "OEF", "OILK", "ONEO", "ONEV", "ONEY", "PCEF", "PCY", "PDBC", "PDP", "PEX", "PFIG", "PGF", "PJP", "PRF", "PRFZ", "PSK", "PSMB", "PSMC", "PSMG", "PSMM", "PUTW", "PXH", "PZA", "QABA", "QCLN", "QEFA", "QEMM", "QTEC", "QUAL", "QUS", "RALS", "REET", "RFAP", "RFDI", "RFEM", "RLY", "RSX", "RWO", "RWR", "RWX", "SCZ", "SFHY", "SHAG", "SHE", "SHM", "SHY", "SHYG", "SIZ", "SJNK", "SKYY", "SLYG", "SLYV", "SMLV", "SPAB", "SPDW", "SPEM", "SPHB", "SPIB", "SPLB", "SPLG", "SPMD", "SPMO", "SPSB", "SPSM", "SPTL", "SPTM", "SPTS", "SPYD", "SPYG", "SPYV", "SPYX", "SRLN", "STIP", "STOT", "STPZ", "SUB", "SYE", "SYG", "SYV", "TDIV", "TFI", "TIP", "TIPX", "TLT", "TOLZ", "USDU", "USFR", "USMF", "USRT", "VB", "VBK", "VBR", "VCIT", "VCLT", "VCSH", "VEA", "VEU", "VGIT", "VGK", "VGLT", "VIG", "VIXM", "VMBS", "VNQ", "VO", "VOE", "VOT", "VRIG", "VRP", "VSS", "VT", "VTI", "VTV", "VUG", "VWO", "VXF", "VYM", "WDTI", "WFHY", "WIP", "XAR", "XHB", "XHE", "XMLV", "XNTK", "XSD", "XSHQ", "XSLV", "XSOE", "XSW", "XTN")
#alltickers = c("AGG", "AUSE", "AXJL", "BIL", "BIV", "BKF", "BLV", "BND", "BSV", "BWX", "CIU", "CSJ", "DBA", "DBC", "DBO", "DBV", "DEM", "DES", "DEW", "DFE", "DFJ", "DGS", "DIM", "DJP", "DLS", "DNL", "DOL", "DON", "DOO", "DRW", "DTH", "DTN", "DWM", "DXJ", "EDV", "EES", "EFG", "EFV", "EMB", "EPI", "EPS", "EWA", "EWC", "EWG", "EWI", "EWJ", "EWP", "EWQ", "EWU", "EWZ", "EXT", "EZM", "EZU", "FBT", "FCG", "FDN", "FEU", "FEZ", "FIW", "FNI", "FVL", "FXD", "FXG", "FXH", "FXI", "FXL", "FXN", "FXO", "FXR", "FXU", "GII", "GMF", "GVI", "GWX", "GXC", "IEI", "IFGL", "IJH", "IJR", "IJS", "ILF", "IOO", "IPE", "ITE", "ITM", "IVE", "IVV", "IWB", "IWD", "IWF", "IWN", "IWO", "IWP", "IWS", "IWV", "JNK", "KBE", "KIE", "LQD", "MBB", "MDYG", "MDYV", "MGK", "MUB", "OEF", "PCY", "PDP", "PGF", "PJP", "PRF", "PRFZ", "PXH", "PZA", "QCLN", "QTEC", "RSX", "RWR", "RWX", "SCZ", "SHM", "SHY", "SLYG", "SLYV", "SPAB", "SPDW", "SPEM", "SPLG", "SPTL", "SPTM", "SPYG", "SPYV", "TFI", "TIP", "TLT", "USRT", "VB", "VBK", "VBR", "VEA", "VEU", "VGK", "VIG", "VNQ", "VO", "VOE", "VOT", "VTI", "VTV", "VUG", "VWO", "VXF", "VYM", "WIP", "XHB", "XSD") 
alltickers = c('AGG', 'AUSE', 'AXJL', 'BIL', 'BIV', 'BLV', 'BND', 'BSV', 'BWX', 'CIU', 'CSJ', 'DBA', 'DBC', 'DBO', 'DBV', 'DEM', 'DES', 'DEW', 'DFE', 'DFJ', 'DGS', 'DIM', 'DJP', 'DLS', 'DNL', 'DON', 'DRW', 'DTH', 'DTN', 'DXJ', 'EDV', 'EES', 'EFG', 'EMB', 'EPI', 'EPS', 'EWA', 'EWC', 'EWG', 'EWI', 'EWJ', 'EWP', 'EWU', 'EWZ', 'EXT', 'EZM', 'FBT', 'FCG', 'FDN', 'FEU', 'FIW', 'FNI', 'FVL', 'FXD', 'FXG', 'FXH', 'FXI', 'FXL', 'FXN', 'FXO', 'FXR', 'FXU', 'GII', 'GMF', 'GVI', 'GWX', 'IEI', 'IFGL', 'IPE', 'ITE', 'ITM', 'IVV', 'JNK', 'KBE', 'KIE', 'LQD', 'MBB', 'MDYG', 'MDYV', 'MUB', 'PCY', 'PDP', 'PGF', 'PJP', 'PXH', 'PZA', 'QCLN', 'QTEC', 'RSX', 'RWR', 'RWX', 'SCZ', 'SHM', 'SHY', 'SLYG', 'SLYV', 'SPAB', 'SPDW', 'SPLG', 'SPTM', 'SPYV', 'TFI', 'TIP', 'TLT', 'USRT', 'VWO', 'WIP', 'XHB', 'XSD')
ReturnsCache = paste(ResultDir, "Returns.RData", sep='/')
if (file.exists(ReturnsCache))
{
  load(ReturnsCache)
} else {
  result = GetReturns(alltickers)
  prices=result$prices
  ret=result$returns
  lengths=result$lengths
  if (!file.exists(ReturnsCache))
  {
    Temp = paste(ResultDir, paste('Temp', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.RData', sep=''), sep='/')
    save(list=c("alltickers", "ret", "prices", "lengths"), file=Temp)
    file.rename(Temp, ReturnsCache)
  }
}
bench = "IVV"
repeat
{
  tickers=names(which(sapply(colnames(ret),function(e) which(alltickers==e, arr.ind=TRUE))>0))
  tickers = c(bench, tickers[tickers != bench])
  ret.filtered = ret[,tickers]
  len.filtered = lengths[tickers]
  ret.cor = cor(ret.filtered)
  ret.highcor = NULL
  for (i in (2:nrow(ret.cor)))
  {
    for (j in (1:(i-1)))
    {
      if (abs(ret.cor[i,j])>0.95)
      {
        ret.highcor = c(ret.highcor, ret.cor[i,j])
        names(ret.highcor)[length(ret.highcor)] = if (len.filtered[i]<=len.filtered[j]) rownames(ret.cor)[i] else colnames(ret.cor)[j]
      }
    }
  }
  if (is.null(ret.highcor))
    break
  alltickers = alltickers[alltickers != names(sort(ret.highcor))[length(ret.highcor)]]
}
write.table(x=paste("alltickers = c('", paste(alltickers, collapse="', '"), "')", sep=""), file="tickers.txt")

ret = ret.filtered
onequarter=63
outofsampledays=onequarter
r = head(ret, nrow(ret) - outofsampledays)
print(dim(r))
minDays = max(round(nrow(r) / 10), ncol(ret) * 2)

asset.names = colnames(r)
maxDays = round(nrow(r)) - outofsampledays
write.table(x=list(maxDays=maxDays, assets=paste(asset.names, collapse=' ')), file=outputfile, col.names=FALSE, append=TRUE)

CalculateObjective = function(result)
{
  return (result$Price.b - result$Price)
}

Objective = function(x, returnScaler=TRUE, debug=TRUE)
{
  delta=x[1]
  days=min(x[2], maxDays)

  if (debug)
  {
    print(paste('Candidate', delta, days))
  }

  CovRSeriesCache = paste(ResultDir, paste('CovRSeries', days, '.RData', sep=''), sep='/')
  if (file.exists(CovRSeriesCache))
  {
    load(CovRSeriesCache)
  }
  else
  {
    param.series = rollapplyr(r, days, by.column=F, function(returns)
    {
      cVaR = apply(returns, 2, NonParametricETL)
      ret.mat = as.matrix(returns)
      colnames(ret.mat) = asset.names
      #print(apply(ret.mat,2,function(r) range(r)))
      #CovR = CovRobust(ret.mat, na.action = na.omit)
      #return (list(Q = CovR@cov, mu = CovR@center, cVaR = cVaR))
      CovR = cov.rob(ret.mat, method="classical")
      return (list(Q = CovR$cov, mu = CovR$center, cVaR = cVaR))
    })
    
    if (!file.exists(CovRSeriesCache))
    {
      Temp = paste(ResultDir, paste('Temp', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.RData', sep=''), sep='/')
      save(list="param.series", file=Temp)
      file.rename(Temp, CovRSeriesCache)
    }
  }
  
  BackTestCache = paste(ResultDir, paste('BackTest', days, '_', delta, '.RData', sep=''), sep='/')
  if (file.exists(BackTestCache))
  {
    load(BackTestCache)
  }
  else
  {
    backtest = meanvar.nosh.rebalance(asset.names, r, param.series, delta)
    
    if (!file.exists(BackTestCache))
    {
      Temp = paste(ResultDir, paste('Temp', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.RData', sep=''), sep='/')
      save(list="backtest", file=BackTestCache)
      file.rename(Temp, BackTestCache)
    }
  }
  
  weights = round(tail(backtest$weights, 1), 5)
  colnames(weights) = asset.names

  if (debug)
  {
    print(weights)
  }
  
  if (returnScaler)
  {
    result = CalcPortfolioStats(r, backtest$weights, backtest$returns)
    
    if (debug)
    {
      print(paste(result$MM, result$Bench, result$Price, result$Price.b))
      
      write.table(x=list(delta=delta, days=days), file=outputfile, col.names=TRUE, append=TRUE)
      write.table(x=weights, file=outputfile, col.names=TRUE, append=TRUE)
      write.table(x=list(IR=round(result$IR, 5), MM=round(result$MM, 5), Bench=round(result$Bench, 5), EndPrice=round(result$Price, 2), EndPrice.b=round(result$Price.b, 2)), file=outputfile, col.names=TRUE, append=TRUE)
    }
    return (CalculateObjective(result))
  }
  else
  {
    return (backtest)
  }
}

GenCandidate = function(params)
{
  delta = params[1]
  days = params[2]
  delta.left = delta / 1.01
  delta.right = delta * 1.01
  
  backtest.left = Objective(c(delta.left, days), returnScaler=FALSE, debug=FALSE)
  backtest = Objective(c(delta, days), returnScaler=FALSE, debug=FALSE)
  backtest.right = Objective(c(delta.right, days), returnScaler=FALSE, debug=FALSE)
  
  result.left = CalcPortfolioStats(r, backtest.left$weights, backtest.left$returns)
  result = CalcPortfolioStats(r, backtest$weights, backtest$returns)
  result.right = CalcPortfolioStats(r, backtest.right$weights, backtest.right$returns)
  
  obj.left = CalculateObjective(result.left)
  obj = CalculateObjective(result)
  obj.right = CalculateObjective(result.right)
  
  if (obj.left > obj && obj < obj.right)
  {
    # Found local minimum for delta, test random start days and print out the result
    weights = apply(backtest$weights, 2, mean)
    scalef = 1 / sum(weights)
    weights.active = weights * scalef
    weights.active[bench] = weights.active[bench] - 1
    print(paste("left=", obj.left, "mid=", obj, "right=", obj.right))
    print(round(weights.active,5))
    w_t = as.Date(sapply(1:nrow(backtest$weights), function(i) time(backtest$weights[i,])))    
    weights.targets = zoo(t(weights.active %o% rep(1,length(w_t))), w_t)
    returns = Return.rebalancing(r, weights.targets)
    test = BackTest(r, weights.targets, returns)
    write.table(x=list(space='=====', IR=round(result$IR, 5), MM=round(result$MM, 5), Excess=round(test$excess, 4), Prob=test$prob, Leverage=round(result$Leverage, 5), delta=delta, days=days), file=outputfile, col.names=TRUE, append=TRUE)
  }
  else
  {
    if (obj.left < obj)
    {
      if (exp(-14) <= delta.right && delta.right <= exp(-4))
      {
        return (c(delta.left, days))
      }
      else
      {
        nextDelta = exp(runif(1,min=-14, max=-4))
        nextDays = round(runif(1,min=minDays, max=maxDays))
        return (c(nextDelta, nextDays))
      }
    }
    else if (obj.right < obj)
    {
      if (exp(-14) <= delta.right && delta.right <= exp(-4))
      {
        return (c(delta.right, days))
      }
      else
      {
        nextDelta = exp(runif(1,min=-14, max=-4))
        nextDays = round(runif(1,min=minDays, max=maxDays))
        return (c(nextDelta, nextDays))
      }
    }
  }
  
  nextDays = round(runif(1,min=minDays, max=maxDays))
  return (c(delta, nextDays))
}

opt = optim(c(0.002, 1148), fn=Objective, gr=GenCandidate, method="SANN", control=list(maxit=10000,trace=F))
