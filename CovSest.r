CovSest = function (x, bdp = 0.5, arp = 0.1, eps = 1e-05, maxiter = 120, 
          nsamp = 500, seed = NULL, trace = FALSE, tolSolve = 1e-14, 
          method = c("sfast", "surreal", "bisquare", "rocke", "suser"), 
          control, t0, S0, initcontrol) 
{
  method <- match.arg(method)
  if (!missing(control)) {
    defcontrol <- CovControlSest()
    if (bdp == defcontrol@bdp) 
      bdp <- control@bdp
    if (arp == defcontrol@arp) 
      arp <- control@arp
    if (eps == defcontrol@eps) 
      eps <- control@eps
    if (maxiter == defcontrol@maxiter) 
      maxiter <- control@maxiter
    if (nsamp == defcontrol@nsamp) 
      nsamp <- control@nsamp
    if (is.null(seed) || seed == defcontrol@seed) 
      seed <- control@seed
    if (trace == defcontrol@trace) 
      trace <- control@trace
    if (tolSolve == defcontrol@tolSolve) 
      tolSolve <- control@tolSolve
    if (method == defcontrol@method) 
      method <- control@method
  }
  if (length(seed) > 0) {
    if (exists(".Random.seed", envir = .GlobalEnv, inherits = FALSE)) {
      seed.keep <- get(".Random.seed", envir = .GlobalEnv, 
                       inherits = FALSE)
      on.exit(assign(".Random.seed", seed.keep, envir = .GlobalEnv))
    }
    assign(".Random.seed", seed, envir = .GlobalEnv)
  }
  if (is.data.frame(x)) 
    x <- data.matrix(x)
  else if (!is.matrix(x)) 
    x <- matrix(x, length(x), 1, dimnames = list(names(x), 
                                                 deparse(substitute(x))))
  xcall <- match.call()
  na.x <- !is.finite(x %*% rep(1, ncol(x)))
  ok <- as.vector(!na.x)
  x <- x[ok, , drop = FALSE]
  dx <- dim(x)
  if (!length(dx)) 
    stop("All observations have missing values!")
  dimn <- dimnames(x)
  n <- dx[1]
  p <- dx[2]
  if (n <= p + 1) 
    stop(if (n <= p) 
      paste("n =", n, "<= p =", p, "-- you can't be serious!")
         else "n == p+1  is too small sample size")
  if (n < 2 * p) {
    warning("n < 2 * p, i.e., possibly too small sample size")
  }
  if (method == "surreal" && nsamp == 500) 
    nsamp = 600 * p
  cobj <- .csolve.bw.S(bdp, p)
  cc <- cobj$cc
  kp <- cobj$kp
  if (trace) 
    cat("\nFAST-S...: bdp, p, cc, kp=", bdp, p, cc, kp, "\n")
  mm <- if (method == "sfast") 
    ..CSloc(x, nsamp = nsamp, kp = kp, cc = cc, trace = trace)
  else if (method == "suser") 
    ..fastSloc(x, nsamp = nsamp, kp = kp, cc = cc, trace = trace)
  else if (method == "surreal") 
    ..covSURREAL(x, nsamp = nsamp, kp = kp, c1 = cc, trace = trace, 
                 tol.inv = tolSolve)
  else if (method == "bisquare") 
    ..covSBic(x, arp, eps, maxiter, t0, S0, nsamp, seed, 
              initcontrol, trace = trace)
  else ..covSRocke(x, arp, eps, maxiter, t0, S0, nsamp, seed, 
                   initcontrol, trace = trace)
  ans <- new("CovSest", call = xcall, iter = mm$iter, crit = mm$crit, 
             cov = mm$cov, center = mm$center, n.obs = n, cc = mm$cc, 
             kp = mm$kp, X = as.matrix(x), method = mm$method)
  ans
}