source("GetReturns.R")
source("MeanVariance.R")
outputfile=paste('TDAmeritradeOptimizeParams', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.dat', sep='')

insertSource("CovSest.R", package = "rrcov", functions = "CovSest")
insertSource("CovMve.R", package = "rrcov", functions = "CovMve")

ResultDir="Result"
dir.create(ResultDir, showWarnings=FALSE)

alltickers = c('AAXJ', 'ACWI', 'AGG', 'AOA', 'AOK', 'AOM', 'AOR', 'BIV', 'BKF', 'BLV', 'BND', 'BSV', 'BWX', 'CIU', 'CSJ', 'DBC', 'DBO', 'DBV', 'DJP', 'EDV', 'EFG', 'EFV', 'EMLC', 'EPI', 'EWA', 'EWC', 'EWG', 'EWI', 'EWJ', 'EWP', 'EWQ', 'EWU', 'EWX', 'EWZ', 'FEZ', 'FXI', 'GUR', 'GVI', 'GXC', 'IEI', 'IJH', 'IJR', 'IJS', 'ILF', 'IOO', 'ITM', 'IVE', 'IVV', 'IWB', 'IWD', 'IWF', 'IWN', 'IWO', 'IWP', 'IWS', 'IWV', 'JNK', 'LQD', 'MBB', 'MGK', 'MUB', 'OEF', 'PCY', 'RSX', 'RWO', 'RWR', 'RWX', 'SCZ', 'SHM', 'SHY', 'STPZ', 'TFI', 'TIP', 'TLT', 'VB', 'VBK', 'VBR', 'VCIT', 'VCLT', 'VCSH', 'VEA', 'VEU', 'VGIT', 'VGK', 'VGLT', 'VGSH', 'VIG', 'VMBS', 'VNQ', 'VO', 'VOE', 'VOT', 'VSS', 'VT', 'VTI', 'VTV', 'VUG', 'VWO', 'VXF', 'VYM', 'WIP')
ReturnsCache = paste(ResultDir, "Returns.RData", sep='/')
if (file.exists(ReturnsCache))
{
  load(ReturnsCache)
} else {
  result = GetReturns(alltickers)
  ret=result$returns
  if (!file.exists(ReturnsCache))
  {
    Temp = paste(ResultDir, paste('Temp', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.RData', sep=''), sep='/')
    save(list=c("alltickers", "ret"), file=Temp)
    file.rename(Temp, ReturnsCache)
  }
}
alltickers=colnames(ret)
bench = "ACWI"
tickers = c(bench, "AAXJ","AGG","AOA","AOK","AOM","AOR","BIV","BKF","BLV","BND","BSV","BWX","CIU","CSJ","DBC","DBO","DBV","DJP","EDV","EFG","EFV","EMLC","EPI","EWA","EWC","EWG","EWI","EWJ","EWP","EWQ","EWU","EWX","EWZ","FEZ","FXI","GUR","GVI","GXC","IEI","IJH","IJR","IJS","ILF","IOO","ITM","IVE","IVV","IWB","IWD","IWF","IWN","IWO","IWP","IWS","IWV","JNK","LQD","MBB","MGK","MUB","OEF","PCY","RSX","RWO","RWR","RWX","SCZ","SHM","SHY","STPZ","TFI","TIP","TLT","VB","VBK","VBR","VCIT","VCLT","VCSH","VEA","VEU","VGIT","VGK","VGLT","VGSH","VIG","VMBS","VNQ","VO","VOE","VOT","VSS","VT","VTI","VTV","VUG","VWO","VXF","VYM","WIP")
validcols = match(tickers,alltickers)
ret.filtered = ret[,validcols[!is.na(validcols)]]
ret.cor = cor(ret.filtered)
ret.highcor.ind = which(abs(ret.cor)>0.90, arr.ind=TRUE)
ret.highcor.ind = ret.highcor.ind[ret.highcor.ind[,1]!=ret.highcor.ind[,2],]
paste(colnames(ret.cor)[ret.highcor.ind[,1]], rownames(ret.cor)[ret.highcor.ind[,2]], ret.cor[ret.highcor.ind])

ret = ret.filtered
onequarter=63
outofsampledays=onequarter
r = head(ret, nrow(ret) - outofsampledays)
print(dim(r))
minDays = round(nrow(r) / 10)

asset.names = colnames(r)
maxDays = round(nrow(r)) - outofsampledays
write.table(x=list(maxDays=maxDays, assets=paste(asset.names, collapse=' ')), file=outputfile, col.names=FALSE, append=TRUE)

CalculateObjective = function(result)
{
  return (result$Price.b - result$Price)
}

Objective = function(x, returnScaler=TRUE, debug=TRUE)
{
  delta=x[1]
  days=min(x[2], maxDays)
  
  if (debug)
  {
    print(paste('Candidate', delta, days))
  }
  
  CovRSeriesCache = paste(ResultDir, paste('CovRSeries', days, '.RData', sep=''), sep='/')
  if (file.exists(CovRSeriesCache))
  {
    load(CovRSeriesCache)
  }
  else
  {
    param.series = rollapplyr(r, days, by.column=F, function(returns)
    {
      cVaR = apply(returns, 2, NonParametricETL)
      ret.mat = as.matrix(returns)
      colnames(ret.mat) = asset.names
      #print(apply(ret.mat,2,function(r) range(r)))
      CovR = CovRobust(ret.mat, na.action = na.omit)
      return (list(Q = CovR@cov, mu = CovR@center, cVaR = cVaR))
      #CovR = cov.rob(ret.mat)
      #return (list(Q = CovR$cov, mu = CovR$center))
    })
    
    if (!file.exists(CovRSeriesCache))
    {
      Temp = paste(ResultDir, paste('Temp', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.RData', sep=''), sep='/')
      save(list="param.series", file=Temp)
      file.rename(Temp, CovRSeriesCache)
    }
  }
  
  BackTestCache = paste(ResultDir, paste('BackTest', days, '_', delta, '.RData', sep=''), sep='/')
  if (file.exists(BackTestCache))
  {
    load(BackTestCache)
  }
  else
  {
    backtest = meanvar.nosh.rebalance(asset.names, param.series, delta)
    
    if (!file.exists(BackTestCache))
    {
      Temp = paste(ResultDir, paste('Temp', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.RData', sep=''), sep='/')
      save(list="backtest", file=BackTestCache)
      file.rename(Temp, BackTestCache)
    }
  }
  
  weights = round(tail(backtest$weights, 1), 5)
  colnames(weights) = asset.names
  
  if (debug)
  {
    print(weights)
  }
  
  if (returnScaler)
  {
    result = CalcPortfolioStats(backtest$weights, backtest$returns)
    
    if (debug)
    {
      print(paste(result$MM, result$Bench, result$Price, result$Price.b))
      
      write.table(x=list(delta=delta, days=days), file=outputfile, col.names=TRUE, append=TRUE)
      write.table(x=weights, file=outputfile, col.names=TRUE, append=TRUE)
      write.table(x=list(IR=round(result$IR, 5), MM=round(result$MM, 5), Bench=round(result$Bench, 5), EndPrice=round(result$Price, 2), EndPrice.b=round(result$Price.b, 2)), file=outputfile, col.names=TRUE, append=TRUE)
    }
    return (CalculateObjective(result))
  }
  else
  {
    return (backtest)
  }
}

GenCandidate = function(params)
{
  delta = params[1]
  days = params[2]
  delta.left = delta - delta/100
  delta.right = delta + delta/100
  
  backtest.left = Objective(c(delta.left, days), returnScaler=FALSE, debug=FALSE)
  backtest = Objective(c(delta, days), returnScaler=FALSE, debug=FALSE)
  backtest.right = Objective(c(delta.right, days), returnScaler=FALSE, debug=FALSE)
  
  result.left = CalcPortfolioStats(backtest.left$weights, backtest.left$returns)
  result = CalcPortfolioStats(backtest$weights, backtest$returns)
  result.right = CalcPortfolioStats(backtest.right$weights, backtest.right$returns)
  
  obj.left = CalculateObjective(result.left)
  obj = CalculateObjective(result)
  obj.right = CalculateObjective(result.right)
  
  if (obj.left > obj && obj < obj.right)
  {
    # Found local minimum for delta, test random start days and print out the result
    weights = apply(backtest$weights, 2, mean)
    scalef = 1 / sum(weights)
    weights.active = weights * scalef
    weights.active[bench] = weights.active[bench] - 1
    print(round(weights.active,5))
    w_t = as.Date(sapply(1:nrow(backtest$weights), function(i) time(backtest$weights[i,])))    
    weights.targets = zoo(t(weights.active %o% rep(1,length(w_t))), w_t)
    returns = Return.rebalancing(r, weights.targets)
    test = BackTest(weights.targets, returns)
    write.table(x=list(space='=====', IR=round(result$IR, 5), MM=round(result$MM, 5), Excess=round(test$excess, 4), Prob=test$prob, Leverage=round(result$Leverage, 5), delta=delta, days=days), file=outputfile, col.names=TRUE, append=TRUE)
  }
  else
  {
    if (obj.left < obj)
    {
      return (c(delta.left, days))
    }
    else
    {
      return (c(delta.right, days))
    }
  }
  
  nextDelta = delta * exp(rnorm(1))
  if (nextDelta < 0.001 || nextDelta > 10)
  {
    nextDelta = runif(1,min=0.001, max=10)
  }
  
  nextDays = round(rnorm(1, mean = days, sd = days/10))
  while (nextDays < minDays || nextDays > maxDays)
  {
    nextDays = round(runif(1,min=minDays, max=maxDays))
  }
  
  return (c(nextDelta, nextDays))
}

opt = optim(c(0.00124458800119131, 939), fn=Objective, gr=GenCandidate, method="SANN", control=list(maxit=10000,trace=F))
