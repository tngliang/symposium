source("GetReturns.R")
source("MeanVariance.R")
outputfile=paste('OptimizeParams', format(Sys.time(), "%b%d%Y"), Sys.getpid(), '.dat', sep='')

insertSource("CovSest.R", package = "rrcov", functions = "CovSest")
insertSource("CovMve.R", package = "rrcov", functions = "CovMve")

outofsampledays=63

tickers = c("AAXJ", "ACWI", "ACWX", "AGG", "AGZ", "BKF", "CMF", "DVY", "DVYE", "EEM", "EEME", "EEMS", "EEMV", "EFA", "EFAV", "EMB", "EWJ", "FLOT", "FM", "GNMA", "GOVT", "GVI", "HYG", "IDV", "IEF", "IEFA", "IEMG", "IEV", "IFGL", "IGOV", "IJH", "IJJ", "IJK", "IJR", "IJS", "IJT", "ILF", "ILTB", "INDY", "ISHG", "ISTB", "ITIP", "ITOT", "IVE", "IVV", "IVW", "IWB", "IWC", "IWD", "IWF", "IWM", "IWN", "IWO", "IWV", "IXUS", "IYR", "LEMB", "LQD", "MBB", "MCHI", "MUB", "NYF", "OEF", "PFF", "PICK", "RING", "SCZ", "SHY", "SLVP", "STIP", "SUB", "TIP", "TLT", "USMV", "VEGI")
ret = GetReturns(tickers)
#r = ret[apply(ret,1,function(row) all(row!=0)),]
r = head(ret,nrow(ret)-outofsampledays)
print(dim(r))

GSPC = getSymbols("^GSPC", auto.assign = FALSE, reload.Symbols = TRUE, from = as.Date("2000-01-01"), to = time(tail(r,1)), verbose = TRUE)
GSPC.ret = as.zoo(na.omit(CalculateReturns(GSPC[,6], method="compound")))

# Badly chosen mean variance portfolio (delta = 0, days = 252)
asset.names = colnames(r)
ret.mat = as.matrix(r)
colnames(ret.mat) = asset.names
CovR = CovRobust(ret.mat, na.action = na.omit)
mv.eff.nosh = meanvar.eff(CovR@center, CovR@cov, asset.names, 100, 0)
mv.eff.tan = mv.eff.nosh[which.max(mv.eff.nosh[,"sharpe"]),]

delta = 0.000126344665976859
days = 164

asset.names = colnames(r)
param.series = rollapplyr(r, days, by.column=F, function(returns)
{
  ret.mat = as.matrix(returns)
  colnames(ret.mat) = asset.names
  CovR = CovRobust(ret.mat, na.action = na.omit)
  return (list(Q = CovR@cov, mu = CovR@center))
})

deltas = c(delta-0.00001, delta, delta+0.00001)
port.ret = sapply(deltas, function(delta)
{
  backtest = meanvar.nosh.rebalance(asset.names, param.series, delta)
  ret = backtest$returns
  ret.sd = sd(ret)
  ret.b = GSPC.ret[time(ret)]
  ret.b.sd = sd(ret.b)
  leverage = ret.b.sd / ret.sd
  return (ret * leverage)
})
port.ret.zoo=zoo(port.ret, as.Date(rownames(port.ret), '%Y-%m-%d'))
ret.comb = merge(GSPC.ret, port.ret.zoo, all=F)
colnames(ret.comb) = c("S&P 500", paste("MV", deltas))

png(paste("BackTestInSample.png"), width=10, height=6, units="in", res=600)
charts.PerformanceSummary(head(ret.comb,nrow(ret.comb-outofsampledays)),
                          colorset=1:ncol(ret.comb),
                          lty=c(1, 1, rep(3,8), 4), lwd=0.5,
                          main="MV Portfolios VS S&P 500",
                          legend.loc="topleft")
dev.off()

png(paste("BackTestOutSample.png"), width=10, height=6, units="in", res=600)
charts.PerformanceSummary(tail(ret.comb,outofsampledays),
                          colorset=1:ncol(ret.comb),
                          lty=c(1, 1, rep(3,8), 4), lwd=0.5,
                          main="MV Portfolios VS S&P 500",
                          legend.loc="topleft")
dev.off()
