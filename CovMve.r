CovMve = function (x, alpha = 1/2, nsamp = 500, seed = NULL, trace = FALSE, 
          control) 
{
  use.correction <- FALSE
  defcontrol <- CovControlMcd()
  if (!missing(control)) {
    if (alpha == defcontrol@alpha) 
      alpha <- control@alpha
    if (nsamp == defcontrol@nsamp) 
      nsamp <- control@nsamp
    if (is.null(seed) || seed == defcontrol@seed) 
      seed <- control@seed
    if (trace == defcontrol@trace) 
      trace <- control@trace
  }
  tolSolve <- defcontrol@tolSolve
  xcall <- match.call()
  if (length(seed) > 0) {
    if (exists(".Random.seed", envir = .GlobalEnv, inherits = FALSE)) {
      seed.keep <- get(".Random.seed", envir = .GlobalEnv, 
                       inherits = FALSE)
      on.exit(assign(".Random.seed", seed.keep, envir = .GlobalEnv))
    }
    assign(".Random.seed", seed, envir = .GlobalEnv)
  }
  if (!missing(nsamp) && is.numeric(nsamp) && nsamp <= 0) 
    stop("Invalid number of trials nsamp = ", nsamp, "!")
  if (is.data.frame(x)) 
    x <- data.matrix(x)
  else if (!is.matrix(x)) 
    x <- matrix(x, length(x), 1, dimnames = list(names(x), 
                                                 deparse(substitute(x))))
  na.x <- !is.finite(x %*% rep(1, ncol(x)))
  ok <- as.vector(!na.x)
  x <- x[ok, , drop = FALSE]
  dx <- dim(x)
  if (!length(dx)) 
    stop("All observations have missing values!")
  dimn <- dimnames(x)
  n <- dx[1]
  p <- dx[2]
  h <- h.alpha.n(alpha, n, p)
  if (n <= p + 1) 
    stop(if (n <= p) 
      paste(head(x),tail(x),"n =", n, "<= p =", p, "-- you can't be serious!")
         else "n == p+1  is too small sample size for MCD")
  if (n < 2 * p) {
    warning("n < 2 * p, i.e., possibly too small sample size")
  }
  if (h > n) 
    stop("Sample size n  <  h(alpha; n,p) := size of \"good\" subsample")
  else if (alpha > 1) 
    stop("alpha must be <= 1")
  raw.cnp2 <- cnp2 <- c(1, 1)
  method <- "Minimum volume ellipsoid estimator"
  mve <- .fastmve(x, h, nsamp)
  mvecov <- cov.wt(x[mve$best, ])
  rcenter <- mvecov$center
  rcov <- mvecov$cov
  mah <- mahalanobis(x, rcenter, rcov, tol = tolSolve)
  calpha <- quantile(mah, h/n)/qchisq(h/n, p)
  names(calpha) <- NULL
  correct <- if (use.correction) 
    (1 + 15/(n - p))^2
  else 1
  raw.cnp2 <- c(calpha, correct)
  rcov <- calpha * correct * rcov
  quantiel <- qchisq(0.975, p)
  mah <- mahalanobis(x, rcenter, rcov, tol = tolSolve)
  weights <- as.numeric(mah < quantiel)
  sum.w <- sum(weights)
  if (sum.w == n) {
    cdelta.rew <- 1
    correct.rew <- 1
  }
  else {
    cdelta.rew <- robustbase:::MCDcons(p, sum.w/n)
    correct.rew <- if (use.correction) 
      1
    else 1
    cnp2 <- c(cdelta.rew, correct.rew)
  }
  xcov <- cov.wt(x, wt = weights)
  xcov$cov <- cdelta.rew * correct.rew * xcov$cov
  raw.mah <- mah
  raw.weights <- weights
  if (-(determinant(xcov$cov, logarithm = TRUE)$modulus[1] - 
    0)/p > 50) {
    if (trace) 
      cat("The reweighted MCD scatter matrix is singular.\n")
    mah <- raw.mah
  }
  else {
    mah <- mahalanobis(x, xcov$center, xcov$cov, tol = tolSolve)
    weights <- as.numeric(mah < quantiel)
  }
  ans <- new("CovMve", call = xcall, iter = nsamp, crit = mve$scale, 
             cov = xcov$cov, center = xcov$center, mah = mah, wt = weights, 
             n.obs = n, X = x, method = method, best = mve$best, alpha = alpha, 
             quan = h, raw.center = rcenter, raw.cov = rcov, raw.mah = raw.mah, 
             raw.wt = raw.weights, raw.cnp2 = raw.cnp2, cnp2 = cnp2)
  ans
}