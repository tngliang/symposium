source("GetReturns.R")
source("MeanVariance.R")

insertSource("CovSest.R", package = "rrcov", functions = "CovSest")
insertSource("CovMve.R", package = "rrcov", functions = "CovMve")

bench = "ACWI"
tickers = c(bench, "AAXJ","AGG","AOA","AOK","AOM","AOR","BIV","BKF","BLV","BND","BSV","BWX","CIU","CSJ","DBC","DBO","DBV","DJP","EDV","EFG","EFV","EMLC","EPI","EWA","EWC","EWG","EWI","EWJ","EWP","EWQ","EWU","EWX","EWZ","FEZ","FXI","GUR","GVI","GXC","IEI","IJH","IJR","IJS","ILF","IOO","ITM","IVE","IVV","IWB","IWD","IWF","IWN","IWO","IWP","IWS","IWV","JNK","LQD","MBB","MGK","MUB","OEF","PCY","RSX","RWO","RWR","RWX","SCZ","SHM","SHY","STPZ","TFI","TIP","TLT","VB","VBK","VBR","VCIT","VCLT","VCSH","VEA","VEU","VGIT","VGK","VGLT","VGSH","VIG","VMBS","VNQ","VO","VOE","VOT","VSS","VT","VTI","VTV","VUG","VWO","VXF","VYM","WIP")
result = GetReturns(tickers)
ret= result$returns

r = ret
outofsampledays=63

leverage = 1

#Objective = Highest Price
#days=939
#days=958
days=989
#days=1052
#days=1336
asset.names = colnames(r)
param.series = rollapplyr(r, days, by.column=F, function(returns)
{
  cVaR = apply(returns, 2, NonParametricETL)
  ret.mat = as.matrix(returns)
  colnames(ret.mat) = asset.names
  CovR = CovRobust(ret.mat, na.action = na.omit)
  return (list(Q = CovR@cov, mu = CovR@center, cVaR = cVaR))
})

ret.comb = r[, bench]
#deltas = c(0, 0.001, 0.00124458800119131 , 0.01, 0.1, 1, 10)
#deltas = c(0, 0.001, 0.00138691227181337, 0.01, 0.1, 1, 10)
deltas = c(0, 0.001, 0.00163965767486545, 0.01, 0.1, 1, 10)
#deltas = c(0, 0.001, 0.00189436343745866, 0.01, 0.1, 1, 10)
#deltas = c(0, 0.001, 0.01, 0.0124273288365223, 0.1, 1, 10)
for (del in deltas)
{
  backtest = meanvar.nosh.rebalance(asset.names, param.series, del)
  weights = apply(backtest$weights, 2, mean)
  scalef = 1 / sum(weights)
  weights.active = weights * scalef
  weights.active[bench] = weights.active[bench] + leverage - 1
  print(round(weights.active,5))
  w_t = as.Date(sapply(1:nrow(backtest$weights), function(i) time(backtest$weights[i,])))    
  weights.targets = zoo(t(weights.active %o% rep(1,length(w_t))), w_t)
  returns = Return.rebalancing(r, weights.targets)
  test = BackTest(weights.targets, returns)
  print(paste("excess=",test$excess,"prob=",test$prob,"del=",del,"days=",days,"leverage=",leverage))
  ret.comb = merge(ret.comb, returns, all=FALSE)
}

colnames(ret.comb) = c(bench, paste("MV Delta =", deltas, " Days =", days, " Leverage =", leverage))

png(paste("TDAmeritrade_BackTestInSample_Active_", bench, "_", days, "_", leverage, ".png", sep=""), width=10, height=6, units="in", res=600)
charts.PerformanceSummary(head(ret.comb, nrow(ret.comb) - outofsampledays),
                          colorset=1:ncol(ret.comb),
                          lty=rep(1,ncol(ret.comb)), lwd=0.5,
                          main="S&P 500 vs MV Portfolios (Objective = Price) In Sample",
                          legend.loc="topleft")
dev.off()

png(paste("TDAmeritrade_BackTestOutOfSample_Active_", bench, "_", days, "_", leverage, ".png", sep=""), width=10, height=6, units="in", res=600)
charts.PerformanceSummary(tail(ret.comb, outofsampledays),
                          colorset=1:ncol(ret.comb),
                          lty=rep(1,ncol(ret.comb)), lwd=0.5,
                          main="S&P 500 vs MV Portfolios (Objective = Price) Out of Sample",
                          legend.loc="topleft")
dev.off()
